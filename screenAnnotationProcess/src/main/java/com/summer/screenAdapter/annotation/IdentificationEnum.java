package com.summer.screenAdapter.annotation;

/**
 * Created by WANG on 2018/9/28.
 */

public enum IdentificationEnum {
    WIDTH,
    HEIGHT,
    IGNORE,
    NULL
}
