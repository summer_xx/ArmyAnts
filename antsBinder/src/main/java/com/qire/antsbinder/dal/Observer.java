package com.qire.antsbinder.dal;

/**
 * Created by Administrator on 2018/3/27.
 */

public interface Observer {
    <T> void update(String tag, T data, boolean isCache);
}
