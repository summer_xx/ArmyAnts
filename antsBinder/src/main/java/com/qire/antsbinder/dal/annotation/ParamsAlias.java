package com.qire.antsbinder.dal.annotation;

import com.qire.antsbinder.dal.ParamsConverter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 参数别名，用于为实现数据访问接口描述其参数使用
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@ParamsWrap
public @interface ParamsAlias {

    /**
     * @return 返回参数的别名，该名字应该与API参数名或用于查询的参数名一样。
     */
    String name();

    /**
     * @return 参数转换器，默认一个空实现，原值传输，可以自己指定一个转换器来进行转换。需要实现 {@link ParamsConverter} 接口。
     */
    Class<? extends ParamsConverter> converter() default ParamsConverter.class;
}
