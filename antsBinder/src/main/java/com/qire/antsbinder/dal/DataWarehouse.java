package com.qire.antsbinder.dal;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * 数据仓库，一个提供可观察的标准数据获取方法的模板类，定义获取数据的标准行为，以及可观察的通知。
 */
public abstract class DataWarehouse {

    public enum MethodType{
        GET,POST
    }

    public enum WarehouseCacheMode{
        /** 按照HTTP协议的默认缓存规则，例如有304响应头时缓存 */
        DEFAULT,

        /** 不使用缓存 */
        NO_CACHE,

        /** 请求网络失败后，读取缓存 */
        REQUEST_FAILED_READ_CACHE,

        /** 如果缓存不存在才请求网络，否则使用缓存 */
        IF_NONE_CACHE_REQUEST,

        /** 先使用缓存，不管是否存在，仍然请求网络 */
        FIRST_CACHE_THEN_REQUEST,
    }

    /**
     * 无效的缓存保存时间
     */
    public static final long InvalidTime = -1;

    private final ArrayList<Observer> observerList = new ArrayList<>();

    /**
     * 获取数据的行为
     * @param url
     * @param map
     * @param methodType
     * @param mWarehouseCacheMode
     * @param cacheTime
     * @param resultType
     */
    public abstract void getData(String url, HashMap<String,Object> map, MethodType methodType, WarehouseCacheMode mWarehouseCacheMode, long cacheTime, Type resultType);

    public abstract void cancel();

    public void bind(Observer observer){
        if(!observerList.contains(observer)) {
            observerList.add(observer);
        }
    }

    public <T>void notice(String tag,T data, boolean isCache){
        for(Observer observer : observerList){
            observer.update(tag,data, isCache);
        }
    }

}
