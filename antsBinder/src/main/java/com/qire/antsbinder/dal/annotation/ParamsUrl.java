package com.qire.antsbinder.dal.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 参数化Url，用于为实现数据访问接口描述其参数使用，作用是标注参数为API接口地址，由于某些特殊情况我们需要动态指定API地址。
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@ParamsWrap
public @interface ParamsUrl {

}
