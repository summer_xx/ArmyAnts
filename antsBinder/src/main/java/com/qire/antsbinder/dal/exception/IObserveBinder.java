package com.qire.antsbinder.dal.exception;

public interface IObserveBinder<T> {

    void register(T observe);

    void unregister();

}
