package com.qire.antsbinder.dal.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 参数化请求头，用于为实现数据访问接口描述其参数使用，作用是标注参数为请求头设置
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@ParamsWrap
public @interface ParamsHead {

    /**
     * @return 返回该参数用于设置的请求头名称
     */
    String headName();

}
