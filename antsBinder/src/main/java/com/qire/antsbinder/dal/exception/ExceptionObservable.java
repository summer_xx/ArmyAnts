package com.qire.antsbinder.dal.exception;

import java.util.HashMap;

/**
 * 数据访问异常被观察者
 */
public final class ExceptionObservable {

    private static final HashMap<Object, ExceptionObserver> observers = new HashMap<>();

    public static void register(Object tag, ExceptionObserver observer) {
        observers.put(tag, observer);
    }

    public static void unregister(Object tag) {
        ExceptionObserver exceptionObserver = observers.get(tag);
        if(exceptionObserver != null){
            observers.remove(tag);
        }
    }

    public static void notifyObservers(DataAccessException exc) {
        if(exc.tag == null) {
            return;
        }
        ExceptionObserver exceptionObserver = observers.get(exc.tag);
        if(exceptionObserver != null) {
            exceptionObserver.onExcReceive(exc);
        }
    }

}
