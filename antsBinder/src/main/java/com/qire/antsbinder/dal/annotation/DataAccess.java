package com.qire.antsbinder.dal.annotation;

import com.qire.antsbinder.dal.DataWarehouse;
import com.qire.antsbinder.dal.DataWarehouse.*;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据访问配置注解，必须指定访问url <br />
 * 1. methodType 默认 {@link MethodType#POST} <br />
 * 2. cacheMode 默认 {@link WarehouseCacheMode#NO_CACHE} <br />
 * 3. cacheTime 默认 {@link DataWarehouse#InvalidTime} <br />
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataAccess {

    String url();

    MethodType methodType() default MethodType.POST;

    WarehouseCacheMode cacheMode() default WarehouseCacheMode.NO_CACHE;

    long cacheTime() default DataWarehouse.InvalidTime;

}
