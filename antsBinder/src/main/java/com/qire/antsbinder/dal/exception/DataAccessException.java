package com.qire.antsbinder.dal.exception;

/**
 * 数据访问异常
 */
public class DataAccessException {
    public final Object tag;
    public String extraMsg;

    public DataAccessException(Object tag) {
        this.tag = tag;
    }
}
