package com.qire.antsbinder.dal.exception;

/**
 * 数据访问异常观察者
 */
public interface ExceptionObserver {
    void onExcReceive(DataAccessException e);
}
