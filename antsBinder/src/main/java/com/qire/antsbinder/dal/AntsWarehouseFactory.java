package com.qire.antsbinder.dal;

import com.qire.antsbinder.DynamicProxyFactory;

public interface AntsWarehouseFactory {

    default <T> T create(Class<T> superClass) {
        return new DynamicProxyFactory(buildWarehouse()).createProxy(superClass);
    }

    AntsDataWarehouse buildWarehouse();

}
