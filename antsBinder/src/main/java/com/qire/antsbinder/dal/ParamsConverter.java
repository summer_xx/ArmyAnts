package com.qire.antsbinder.dal;

/**
 * 请求参数转换器，在某些时刻接口接受的参数类型或形式与本地不一样，代码在阅读和理解过程中使用更方便可以使用这个接口作为转换。
 * 如MD5加密，此处可以考虑提供转换器功能； <br />
 * 接口实现类必须提供一个空的构造类型，否则会报错。
 * @param <T> 输出目标类型
 * @param <V> 输入值类型
 */
public interface ParamsConverter<T, V> {
    T change(V value);
}
