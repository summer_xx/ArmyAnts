package com.qire.antsbinder.dal.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 策略注解，用于数据服务接受通知后分发给不同观察者订阅策略使用。
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface StrategyAnnotation {
    /**
     * 关注事件
     * **/
    String[] event();

    /**
     * 通知使用的线程模式，默认在原线程中回调。*/
    ThreadMode threadMode() default ThreadMode.POSTING;

    /**
     * 运行的线程名*/
    String threadAlias() default "";

    /**
     * 通知使用的线程模式，接口回调后，通知在那个线程中运行调用。
     */
    enum ThreadMode{
        /**原调用线程中执行*/
        POSTING,
        /**强制主线程中执行*/
        MAIN,
        /**强制异步子线程中执行*/
        ASYNC
    }
}
