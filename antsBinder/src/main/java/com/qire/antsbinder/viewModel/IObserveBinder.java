package com.qire.antsbinder.viewModel;

// todo: 这个接口可以考虑不用，因为可以使用静态方法来调用效果一样，目前还是加上
public interface IObserveBinder<T> {
    void observeBind(AntsViewModel<?> viewModel,T observe);
}
