package com.qire.antsbinder.viewModel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用来描述扩展ViewModel属性关联行为的注解，通过该注解可以是接口行为具备特定的对ViewModel属性GET，SET的能力<br />
 * <pre>
 *     public interface IHomeViewModel {
 *      ＠ViewModelProperty(name="msg")
 *      MutableLiveData &lt;String&gt; getMsg();
 *     }
 * </pre>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface ViewModelProperty {

    /**
     * 绑定ViewModel属性字段名称，不允许为空：<br />
     * 1.{@link ViewModelProperty#type() type()} 为{@link Type#GET GET}时，只有第一个名称有效。<br />
     * 2.{@link ViewModelProperty#type() type()} 为{@link Type#SET SET}时，属性名称应该与方法参数数量一致，且一一对应。<br />
     * @return 包含需要操作的ViewModel属性名称数组
     */
    String[] name();

    /**
     * 操作类型
     * @return 类型枚举
     */
    Type type() default Type.GET;

    /**
     * ViewModel操作类型枚举，用来描述注解标记的方法是用来获取名称对应字段属性，还是设置名称对应字段属性值。
     */
    enum Type {
        /**
         * 标记从ViewModel中获取对应属性名的值
         */
        GET,
        /**
         * 标记设置ViewModel中对应属性名的值
         */
        SET
    }
}
