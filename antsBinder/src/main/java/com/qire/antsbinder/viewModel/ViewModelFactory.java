package com.qire.antsbinder.viewModel;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProvider.NewInstanceFactory;
import androidx.lifecycle.ViewModelStoreOwner;

public class ViewModelFactory {

    public static <T extends AntsViewModel> T crate(ViewModelStoreOwner storeOwner, Class<T> viewModelCls) {
        return new ViewModelProvider(storeOwner,new AntsInstanceFactory()).get(viewModelCls);
    }

    private static final class AntsInstanceFactory extends NewInstanceFactory{
        // 需要接管原始构建方法可以在这里实现，比如，改变构建的存储机制
    }

}
