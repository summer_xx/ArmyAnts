package com.qire.common.function;


@FunctionalInterface
public interface TaskAction<T> {

    void accept(T t);

    default void waitNext() {
        try {
            synchronized (this) {
                this.wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    default void notifyNext() {
        synchronized (this) {
            this.notifyAll();
        }
    }

}