package com.qire.common.basic;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串处理工具*/
public class StringUtil {

    /**
     * 字符串是否为空或长度为0*/
    public static boolean isEmpty(CharSequence str){
        return str == null || str.length() == 0;
    }
    /**
     * 字符串是否为空或长度为0*/
    public static boolean isEmpty(String str){
        return str == null || str.length() == 0;
    }
    /**
     * 字符串不为null且长度也不为0
     */
    public static boolean notEmpty(CharSequence str){
        return !isEmpty(str);
    }

    /**
     * 检查源字串中包含目标字串，源或目标其中任意一个为null 都视为不包含
     * @param src 源字串
     * @param dst 目标字串
     * @return 源字串中包含目标字串返回 true 否则返回 false，如果源或目标其中一个为null则返回false
     */
    public static boolean contains(String src, String dst) {
        if(src == null || dst == null) {
            return false;
        }
        return src.contains(dst);
    }

    /**
     * 检查源字串中不包含目标字串，源或目标其中任意一个为null 都视为不包含
     * @param src 源字串
     * @param dst 目标字串
     * @return 源字串中不包含目标字串返回 true 否则返回 false，如果源或目标其中一个为null则返回true
     */
    public static boolean notContains(String src, String dst) {
        return !contains(src, dst);
    }

    /**
     * 使用源字串从指定位置开始覆盖目标字串指定位置开始的内容并得到一个新的字符串
     * @param src 源字符串
     * @param dst 目标字符串
     * @param start 指定替换开始位置
     * @return 不会裁剪新生成字串
     */
    public static String replace(String src, String dst, int start) {
        return replace(src, dst, start, 0);
    }

    /**
     * 使用源字串从指定位置开始覆盖目标字串指定位置开始的内容并得到一个新的字符串
     * @param src 源字符串
     * @param dst 目标字符串
     * @param start 指定替换开始位置
     * @param cutStart 从上面位置开始裁剪生成字串，保留起始到 cutStart 位置的字串，如果 cutStart 为 0 则不裁剪
     * @return 新的替换后的字符串: src -> "2020-10-11 1"  dst -> "0000-00-00 23:59:59"  =  2020-10-11 13:59:59
     */
    public static String replace(String src, String dst, int start, int cutStart) {
        if(start >= dst.length()) {
            return dst;
        }

        int end = start + src.length();
        StringBuilder builder = new StringBuilder(dst);
        builder.replace(start, end, src);

        if(cutStart > 0 && cutStart < builder.length()) {
            return builder.substring(0, cutStart);
        }

        return builder.toString();
    }

    /**
     * 缩写
     * @param src 源字串
     * @param limit 字符长度
     * @param suffix 后缀
     * @return
     */
    public static String abbreviation(String src, int limit, String suffix) {
        if(src.length() > limit) {
            return src.substring(0, limit) + suffix;
        }
        return src;
    }

    /**
     * 判定字符串是否为手机号码*/
    public static boolean isMobile(String number) {
        Pattern pattern = Pattern.compile("^0?1[3|4|5|7|8][0-9]\\d{8}$");
        Matcher matcher = pattern.matcher(number);
        return matcher.find();
    }

    /**
     * 判定字符串是否为邮箱*/
    public static boolean isEmail(String number) {
        Pattern pattern = Pattern.compile("^([\\.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\\.[a-zA-Z0-9_-])+");
        Matcher matcher = pattern.matcher(number);
        return matcher.find();
    }

    /**
     * 从全时间格式里截取日期
     * @param fullDateString
     * @return */
    public static String extractTheDate(String fullDateString){
        if(fullDateString != null && fullDateString.length() > 10) {
            return fullDateString.substring(0, 10);
        }
        return fullDateString;
    }

    /**
     * 格式化存储大小单位
     * @param storageSize 存储大小,字节数
     * @return 格式化后带单位的存储大小
     */
    public static String formatStorageSize(long storageSize) {
        // 内存机制进位权值 1024 ， 为了计算小数固位Double
        double weight = 1024d;
        // 进制单位
        String[] units = {"B", "KB", "MB", "GB", "TB"};
        // 格式化后小数大小
        double byteSize = storageSize;
        // 格式化结果
        String format = storageSize + units[0];

        for(String unit : units) {
            format = new BigDecimal(byteSize).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            format = subZeroAndDot(format) + unit;
            byteSize = byteSize / weight;
            if(byteSize < 1) {
                break;
            }
        }

        return format;
    }


    public static String toSymbol(int in) {

        if(in > 100000000){
            return String.format(Locale.US, "%.2f%s", in / 100000000.0f, "亿");
        } if(in > 10000){
            String tenThousand = String.format(Locale.US, "%.1f", in / 10000.0f);
            tenThousand = subZeroAndDot(tenThousand);
            return tenThousand + "万";
//        } else if(in > 1000){
//            return String.format(Locale.US, "%.2f%s", in / 1000.0f, "千");
        } else {
            return in + "";
        }
    }

    /**
     * 使用java正则表达式去掉多余的.与0
     * @param s
     * @return
     */
    public static String subZeroAndDot(String s){
        if(s.indexOf(".") > 0){
            s = s.replaceAll("0+?$", "");//去掉多余的0
            s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
        }
        return s;
    }

    /**
     * 计算当前到给定时间的差并格式化成对用户友好的天数
     * @param pastTime
     * @return */
    public static String friendlyDay(long pastTime){
        long currentTime = System.currentTimeMillis() / 1000;       // 换成秒
        if(pastTime > currentTime){
            pastTime /= 1000;               // 传过来可能为毫秒，换成秒
        }
        long intervals = currentTime - pastTime;
        int day = (int) (intervals / (24 * 60 * 60));
        if(day == 0){
            return "今日";
        }
        if(day == 1){
            return "昨天";
        }
        if(day > 6 ) day = 7;
        return day + "天前";
    }

    public static String friendlyDay(Date date){
        return friendlyDay(date.getTime());
    }

    public static String formatFloat(float f){
        DecimalFormat decimalFormat=new DecimalFormat(".00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
        return decimalFormat.format(f);
    }

    /**
     * 是否是数字（包括小数）
     * @param str
     * @return 是则返回true否则false
     */
    public static boolean isNumeric(String str){
        int dotCount = 0;
        for(int i=str.length();--i>=0;){
            char singleChar =  str.charAt(i);
            if('.' == singleChar){
                dotCount++;
                if(dotCount<=1)
                    continue;
                else
                    return false;
            }
            if (!Character.isDigit(singleChar)){
                return false;
            }
        }
        return true;
    }

    public static Float valueToFloat(String value, float def) {
        if(isEmpty(value))
            return def;
        if(!isNumeric(value))
            return def;
        return new BigDecimal(value).floatValue();
    }

    public static int valueToInt(Object value, int def) {
        if(value != null) {
            return valueToInt(value.toString(), def);
        }
        return def;
    }
    public static int valueToInt(String value, int def) {
        if(isEmpty(value))
            return def;
        if(!isNumeric(value))
            return def;
        return new BigDecimal(value).intValue();
    }

    public static String defValue(String value, String def) {
        return isEmpty(value) ? def : value;
    }

}
