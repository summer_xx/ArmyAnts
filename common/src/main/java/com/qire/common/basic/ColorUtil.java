package com.qire.common.basic;

import android.graphics.Color;

public enum ColorUtil {

    RGB() {
        public final String from(int color) {
            String colorStr =  Integer.toHexString(0xff000000 | color);
            return "#" + colorStr.substring(colorStr.length() - 6);
        }
    },
    ARGB() {
        public final String from(int color) {
            String colorStr =  Long.toHexString(0xff00000000000000L | color);
            return "#" + colorStr.substring(colorStr.length() - 8);
        }
    };

    ColorUtil() {
    }

    public abstract String from(int color);

    /**
     * 颜色加深算法
     */
    public final int colorBurn(int color, float val) {
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        red = (int) Math.floor(red * (1f - val));
        green = (int) Math.floor(green * (1f - val));
        blue = (int) Math.floor(blue * (1f - val));
        return Color.rgb(red, green, blue);
    }

}
