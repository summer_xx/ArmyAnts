package com.qire.common.utils;

import android.util.Log;

import com.qire.common.BuildConfig;


/**
 * 简单日志工具
 */
public class Logger {

    private static String   TAG     = BuildConfig.APPLICATION_ID;
    private static boolean  DEBUG   = BuildConfig.DEBUG;

    private Logger() {
        Logger.d("调试消息");
        Logger.e("错误消息");
        Logger.i("普通消息");
        Logger.v("无意义的消息");
        Logger.w("警告消息");
    }

    public static void setTAG(String TAG) {
        Logger.TAG = TAG;
    }

    /**
     * 输出颜色为黑色的，任何消息都会输出，这里的v代表verbose啰嗦的意思
     * @param msg 消息
     */
    public static void v(String msg) {
        if (DEBUG) {
            Log.v(TAG, msg);
        }
    }

    /**
     * 输出颜色为蓝色的，仅输出debug调试的意思，但他会输出上层的信息，过滤起来可以通过DDMS的Logcat标签来选择。
     * @param msg 消息
     */
    public static void d(String msg) {
        if (DEBUG) {
            Log.d(TAG, msg);
        }
    }

    /**
     * 输出颜色为绿色，一般提示性的消息information，它不会输出Log.v和Log.d的信息，但会显示i、w和e的信息。
     * @param msg 消息
     */
    public static void i(String msg) {
        if (DEBUG) {
            Log.i(TAG, msg);
        }
    }

    /**
     * 输出颜色为橙色，可以看作为warning警告，一般需要我们注意优化Android代码，同时选择它后还会输出Log.e的信息。
     * @param msg 消息
     */
    public static void w(String msg) {
        if (DEBUG) {
            Log.w(TAG, msg);
        }
    }

    /**
     * 输出颜色为红色，可以想到error错误，这里仅显示红色的错误信息，这些错误就需要我们认真的分析，查看栈的信息了。
     * @param msg 消息
     */
    public static void e(String msg) {
        if (DEBUG) {
            Log.e(TAG, msg);
        }
    }

}
