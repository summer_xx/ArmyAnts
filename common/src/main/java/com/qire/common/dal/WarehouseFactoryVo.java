package com.qire.common.dal;

import com.qire.antsbinder.dal.AntsDataWarehouse;
import com.qire.antsbinder.dal.AntsWarehouseFactory;
import com.qire.antsbinder.dal.Observer;
import com.qire.antsbinder.viewModel.AntsViewModel;
import com.qire.common.dal.base.BaseNetworkAccessWarehouse;
import com.qire.common.support.base.BaseVO;

public class WarehouseFactoryVo implements AntsWarehouseFactory {

    private final AntsViewModel viewModel;

    private WarehouseFactoryVo(AntsViewModel viewModel){
        this.viewModel = viewModel;
    }

    public static <T> T create(AntsViewModel observer, Class<T> superClass) {
        return new WarehouseFactoryVo(observer).create(superClass);
    }

    @Override
    public AntsDataWarehouse buildWarehouse() {
        AntsDataWarehouse warehouse = new BaseNetworkAccessWarehouse();
        warehouse.bind(new Observer(){
            @Override
            public <T> void update(String tag, T data, boolean isCache) {
                ((BaseVO)data).updateModel(viewModel);
            }
        });
        return warehouse;
    }
}
