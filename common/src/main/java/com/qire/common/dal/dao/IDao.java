package com.qire.common.dal.dao;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Update;

public interface IDao<E> {

    /**
     * 插入一个或多个实体到数据库
     * @param entity 数据实体列表
     */
    @Insert
    void insert(E... entity);

    /**
     * 插入一个或多个实体到数据库，如果遇到建立了唯一索引的字段相同的记录会被覆盖(删除原来的，追加一条新的，这可能导致自增ID主键的记录ID会变化增长)
     * @param entity
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertReplace(E... entity);

    /**
     * 更新一条或多记录到数据
     * @param entity
     */
    @Update
    void update(E... entity);

    /**
     * 删除一条或多条记录到数据库
     * @param entity
     */
    @Delete
    void delete(E... entity);

}
