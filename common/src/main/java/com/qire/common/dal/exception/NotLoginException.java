package com.qire.common.dal.exception;

import com.qire.antsbinder.dal.exception.DataAccessException;

/**
 * 接口返回未登录代码时抛出
 */
public class NotLoginException extends DataAccessException {
    public String requestUrl;

    public NotLoginException(Object tag) {
        super(tag);
    }
}
