package com.qire.common.dal;

import com.qire.antsbinder.dal.AntsDataWarehouse;
import com.qire.antsbinder.dal.AntsWarehouseFactory;
import com.qire.antsbinder.dal.Observer;
import com.qire.common.dal.base.BaseNetworkAccessWarehouse;

public class WarehouseFactory implements AntsWarehouseFactory {

    private final Observer observer;

    private WarehouseFactory(Observer observer){
        this.observer = observer;
    }

    public static <T> T create(Observer observer, Class<T> superClass) {
        return new WarehouseFactory(observer).create(superClass);
    }

    @Override
    public AntsDataWarehouse buildWarehouse() {
        AntsDataWarehouse warehouse = new BaseNetworkAccessWarehouse();
        warehouse.bind(observer);
        return warehouse;
    }
}
