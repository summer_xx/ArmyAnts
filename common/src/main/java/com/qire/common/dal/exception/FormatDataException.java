package com.qire.common.dal.exception;

import com.qire.antsbinder.dal.exception.DataAccessException;

/**
 * json格式化异常，解析json失败json格式存在问题时抛出
 */
public class FormatDataException extends DataAccessException {
    public FormatDataException(Object tag) {
        super(tag);
    }
}
