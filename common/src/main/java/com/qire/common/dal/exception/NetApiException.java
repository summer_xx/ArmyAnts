package com.qire.common.dal.exception;

import com.qire.antsbinder.dal.exception.DataAccessException;

/**
 * 接口业务异常，服务器调用返回业务错误代码时抛出
 */
public class NetApiException extends DataAccessException {

    // 异常来源信息
    public String requestUrl;
    public int errorCode;

    public NetApiException(Object tag) {
        super(tag);
    }
}
