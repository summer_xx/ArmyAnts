package com.qire.common.dal.dao;

import com.qire.common.SummerApp;
import com.qire.common.basic.ObjectUtil;

import java.util.HashMap;

import androidx.room.Room;

public final class DatabaseFactory {

    private static final DatabaseFactory FACTORY = new DatabaseFactory();

    private final String DB_SUFFIX = ".db";

    // volatile
    private final HashMap<String, BaseDatabase> databaseMapper = new HashMap<>();

    private DatabaseFactory() {}

    public static synchronized <T extends BaseDatabase> T create(Class<T> databaseClass) {
        String nameKey = databaseClass.getCanonicalName();
        T database = (T) FACTORY.databaseMapper.get(nameKey);
        if(ObjectUtil.isNull(database)) {
            database = Room.databaseBuilder(SummerApp.summerApp(), databaseClass, nameKey + FACTORY.DB_SUFFIX).build();
            FACTORY.databaseMapper.put(nameKey, database);
        }
        return database;
    }

}
