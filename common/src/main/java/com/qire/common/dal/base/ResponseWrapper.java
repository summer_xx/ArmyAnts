package com.qire.common.dal.base;

import com.qire.antsbinder.dal.exception.ExceptionObservable;
import com.qire.common.dal.exception.NetApiException;
import com.qire.common.dal.exception.NotLoginException;
//import com.qire.common.model.entity.UserEntity;

import java.io.Serializable;

public class ResponseWrapper implements Serializable {

    private static final long serialVersionUID = 1;

    /**
     * 错误代码：未登录
     */
    public static final int EC_NOT_LOGIN = 10005;
    /**
     * 错误代码：账户被禁用
     */
    public static final int EC_ACCOUNT_DISABLE = 11111;

    private int state;
    private int status;
    private String req;
    private String msg;
    private Object data;
    private String logid;
    private double usetime;
    private int errCode;

    public int getState() {
        return state;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public Object getData() {
        return data;
    }

    public boolean isIllegal(){
        return getState() == 0 || getStatus() < 1;
    }

    /**
     * 该方法是当请求失败时调用，isIllegal返回true时，需要更具服务器返回的错误代码进行处理，或者是其他方式处理错误情况*/
    public void illegalExceptionHandle(String tagUrl){
        switch (errCode) {
            case EC_NOT_LOGIN :
                NotLoginException notLoginException = new NotLoginException(tagUrl);
                notLoginException.requestUrl = tagUrl;
                notLoginException.extraMsg = getMsg();
                ExceptionObservable.notifyObservers(notLoginException);
                return;
            case EC_ACCOUNT_DISABLE :
//                UserEntity.self.logout();
                // todo: 需要跳转提示
                return;
            default :
                NetApiException netApiException = new NetApiException(tagUrl);
                netApiException.requestUrl = tagUrl;
                netApiException.errorCode = errCode;
                netApiException.extraMsg = getMsg();
                ExceptionObservable.notifyObservers(netApiException);
                return;
        }
    }

}
