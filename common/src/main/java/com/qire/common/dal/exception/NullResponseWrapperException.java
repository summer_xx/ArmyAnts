package com.qire.common.dal.exception;

import com.qire.antsbinder.dal.exception.DataAccessException;

/**
 * 返回请求是未获得响应时抛出
 */
public class NullResponseWrapperException extends DataAccessException {

    public String requestUrl;

    public NullResponseWrapperException(Object tag) {
        super(tag);
    }
}
