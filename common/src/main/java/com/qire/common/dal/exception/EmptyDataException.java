package com.qire.common.dal.exception;

import com.qire.antsbinder.dal.exception.DataAccessException;

/**
 * 空数据异常，返回body()数据为null抛出
 * */
public class EmptyDataException extends DataAccessException {
    public EmptyDataException(Object tag) {
        super(tag);
    }
}
