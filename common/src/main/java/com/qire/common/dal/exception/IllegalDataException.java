package com.qire.common.dal.exception;

import com.qire.antsbinder.dal.exception.DataAccessException;

/**
 * 非法数据异常，
 */
public class IllegalDataException extends DataAccessException {

    public IllegalDataException(Object tag) {
        super(tag);
    }
}
