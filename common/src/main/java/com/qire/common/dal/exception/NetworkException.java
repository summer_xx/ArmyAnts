package com.qire.common.dal.exception;

import com.qire.antsbinder.dal.exception.DataAccessException;

/**
 * Http请求出现错误时抛出
 * */
public class NetworkException extends DataAccessException {

    // 异常来源信息
    public String requestUrl;

    public NetworkException(Object object){
        super(object);
    }

}
