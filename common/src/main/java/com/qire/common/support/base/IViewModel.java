package com.qire.common.support.base;

/**
 * 原本为了注入提供一个variableId()方法契约，是其子接口都是用def默认方法实现这个行为，而获得统一获取variableId的行为，
 * 由于动态代理解决调用默认方法的行为需要支持android26以上版本所以不在使用，保留接口的目的是为了以后扩展统一行为留空间
 */
public interface IViewModel {

//    // 使用默认方法投递BindView的variableId,但由于默认方法的动态代理处理需要支持到android 26以上目前放弃
//    int variableId();

}
