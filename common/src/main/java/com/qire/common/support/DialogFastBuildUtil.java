package com.qire.common.support;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.qire.antsrouter.lifecycleHandler.ActivityManageHandler;
import com.qire.common.basic.ObjectUtil;
import com.qire.common.basic.StringUtil;
import com.qire.common.support.base.BaseDialog;

/**
 * 通用对话框快速构建工具
 */
public class DialogFastBuildUtil {

    /** 保存Dialog对象的标签 */
    private static final int SAVE_DIALOG_TAG = (8 << 24);

    public static Builder builder(int layoutId) {
        return new Builder(layoutId);
    }

    public static Builder builder(Activity activity, int layoutId) {
        return new Builder(activity, layoutId);
    }

    public static void dismissDialogBy(View view) {
        BaseDialog dialog = findDialogBy(view);
        if(ObjectUtil.nonNull(dialog)) {
            dialog.dismiss();
        }
    }

    public static <D extends BaseDialog> D findDialogBy(View view) {
        Object obj = view.getTag(SAVE_DIALOG_TAG);
        try {
            return (D) BaseDialog.class.cast(obj);
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static class Builder/*<VDB extends ViewDataBinding>*/ {

        private final BaseDialog dialog;
//        private final VDB viewDataBinder;

        private Builder(int layoutId) {
            this(ActivityManageHandler.HANDLER.currentActivity(), layoutId);
        }

        private Builder(Activity activity, int layoutId) {
            dialog = new BaseDialog(activity);
            dialog.setContentView(layoutId);

//            View rootView = LayoutInflater.from(activity).inflate(layoutId, null);
//            dialog.setContentView(rootView);
//            viewDataBinder = DataBindingUtil.bind(rootView);
        }

//        public Builder setVariable(int variableId, Object variable) {
//            viewDataBinder.setVariable(variableId, variable);
//            return this;
//        }
//
//        public Builder callAction(Consumer<VDB> action) {
//            if(ObjectUtil.nonNull(action)) {
//                action.accept(viewDataBinder);
//            }
//            return this;
//        }

        public Builder setTextHint(int rid, CharSequence hintText) {
            TextView textView = findView(rid);
            if(StringUtil.notEmpty(hintText)) {
                textView.setHint(hintText);
            }
            return this;
        }

        public Builder setTextView(int rid, View.OnClickListener listener) {
            return setTextView(rid, null, listener);
        }
        public Builder setTextView(int rid, CharSequence btnText) {
            return setTextView(rid, btnText, null);
        }
        public Builder setTextView(int rid, CharSequence btnText, View.OnClickListener listener) {
            TextView textView = findView(rid);
            if(StringUtil.notEmpty(btnText)) {
                textView.setText(btnText);
            }
            textView.setOnClickListener(listener);
            return this;
        }

        public <V extends View> V findView(int rid) {
            V view = dialog.findViewById(rid);
            view.setTag(SAVE_DIALOG_TAG, dialog);
            return view;
        }

        public Builder show(){
            dialog.show();
//            wrapParent(dialog.getWindow());
            return this;
        }
        public Builder dismiss(){
            dialog.dismiss();
            return this;
        }

//        private void wrapParent(Window window){
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
//        }

    }

}
