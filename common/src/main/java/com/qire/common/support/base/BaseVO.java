package com.qire.common.support.base;

import com.qire.antsbinder.viewModel.AntsViewModel;
import com.qire.common.dal.WarehouseFactoryVo;

/**
 * 统一调用提供统一回调行为，
 * @see WarehouseFactoryVo
 */
public class BaseVO<T extends AntsViewModel> {

    public void updateModel(T viewModel) {

    }

}
