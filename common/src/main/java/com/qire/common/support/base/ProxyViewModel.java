package com.qire.common.support.base;

/**
 * 默认代理视图模型，为配合接口注入而存在的代理模型
 * @param <P> 目标接口
 */
public final class ProxyViewModel<P extends IViewModel> extends BaseViewModel<P> {

}
