package com.qire.common.support;

import android.content.DialogInterface.OnCancelListener;

import com.qire.antsrouter.lifecycleHandler.ActivityManageHandler;
import com.qire.common.support.base.BaseViewModel;
import com.qire.common.support.base.IViewModel;

public abstract class AbsBringDialogViewModel<P extends IViewModel> extends BaseViewModel<P> {

    public void showLoadDialog() {
        showLoadDialog("");
    }

    public void showLoadDialog(String messageText) {
        showLoadDialog(messageText, false, null);
    }

    public void showLoadDialog(CharSequence message, boolean cancelable, OnCancelListener cancelListener) {
//        CustomLoadDialog.show(ActivityManageHandler.HANDLER.currentActivity(), message, cancelable, cancelListener);
    }

    public void dismissLoadDialog() {
//        CustomLoadDialog.dismissDialog();
    }

}
