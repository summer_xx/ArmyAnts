//package com.qire.common.support;
//
//import com.qire.common.support.base.BaseViewModel;
//import com.qire.common.support.base.IViewModel;
//import com.qire.common.widget.ReferBinder;
//import com.scwang.smart.refresh.layout.SmartRefreshLayout;
//import com.scwang.smart.refresh.layout.constant.RefreshState;
//
//public abstract class SmartRefreshViewModel<P extends IViewModel> extends BaseViewModel<P> {
//
//    private final ReferBinder<SmartRefreshLayout> refreshLayoutBinder = new ReferBinder<>();
//
//    public ReferBinder<SmartRefreshLayout> getRefreshLayoutBinder() {
//        return refreshLayoutBinder;
//    }
//
//    public void refreshLayoutFinish(boolean success) {
//        SmartRefreshLayout refreshLayout = refreshLayoutBinder.get();
//        if(refreshLayout == null) {
//            return;
//        }
//        if(refreshLayout.getState() == RefreshState.Refreshing) {
//            refreshLayout.finishRefresh(success);
//        } else {
//            refreshLayout.finishLoadMore(success);
//        }
//    }
//
//    public void refreshLayoutFinish(int delayed) {
//        SmartRefreshLayout refreshLayout = refreshLayoutBinder.get();
//        if(refreshLayout == null) {
//            return;
//        }
//        if(refreshLayout.getState() == RefreshState.Refreshing) {
//            refreshLayout.finishRefresh(delayed);
//        } else {
//            refreshLayout.finishLoadMore(delayed);
//        }
//    }
//
//    public void noMoreDataWasShown() {
//        SmartRefreshLayout refreshLayout = refreshLayoutBinder.get();
//        if(refreshLayout == null) {
//            return;
//        }
//        refreshLayout.setNoMoreData(true);
//        refreshLayout.postDelayed(() -> refreshLayout.setNoMoreData(false), 600);
//    }
//
//}
