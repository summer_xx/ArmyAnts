package com.qire.common.support.base;

import com.qire.antsbinder.viewModel.AntsViewModel;
import com.qire.common.inject.ViewModelInjector;
import com.qire.common.utils.ObserveBinderHelper;

/**
 * 基础ViewModel 继承自 AntsViewModel
 * @param <P> 代理接口用来扩展BindView中使用的get、set方法，目前是空接口
 * @see IViewModel
 */
public abstract class BaseViewModel<P extends IViewModel> extends AntsViewModel<P> {

    /** 无效的ID */
    public static final int INVALID_ID = -1;

    public BaseViewModel() {
        ObserveBinderHelper.callObserveBindOfPropertyObserveBinder(this,this);
    }

    /**
     * 用于注入到 binder 中时对应XML的 variableId，依赖注入时需要使用，如果使用接口则在接口中指定
     * 如果不使用接口则需要在子类重写改方法并且提供 variableId
     * @see androidx.databinding.ViewDataBinding#setVariable(int, Object)
     * @return 如果返回结果 {@link #INVALID_ID} 那么 {@link ViewModelInjector#inject} 则会反射获取 {@link IViewModel} 中的 variableId变量；
     * 所以务必保证两个中存在一个指定了 variableId
     */
    public int getVariableId() {
        return INVALID_ID;
    }

    @Override
    public P buildProxy() {
        if(getVariableId() != INVALID_ID) {
            return (P) this;
        }
        return super.buildProxy();
    }
}