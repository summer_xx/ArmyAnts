//package com.qire.common.support;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.WindowManager;
//
//import com.qire.common.R;
//import com.qire.common.basic.StringUtil;
//import com.qire.common.databinding.DialogCustomLoadBinding;
//import com.qire.common.support.base.BaseDialog;
//
//import androidx.annotation.MainThread;
//import androidx.databinding.ObservableField;
//import androidx.databinding.ObservableInt;
//
///**
// * 自定义加载弹窗
// * @author summer
// */
//public class CustomLoadDialog extends BaseDialog {
//
//	private static CustomLoadDialog customLoadDialog;
//
//	/**
//	 * 弹出一个自定义 无关闭监听、无法手动关闭的 CustomLoadDialog
//	 * @param context	上下文
//	 * @param message	提示
//	 * @return
//	 */
//	public static CustomLoadDialog show(Context context, CharSequence message) {
//		return show(context, message, false);
//	}
//
//	/**
//	 * 弹出一个自定义 无关闭监听的 CustomLoadDialog
//	 * @param context 上下文
//	 * @param message 提示
//	 * @param cancelable 是否按返回键取消
//	 * @return
//	 */
//	public static CustomLoadDialog show(Context context, CharSequence message, boolean cancelable) {
//		return show(context, message, cancelable, null);
//	}
//
//	/**
//	 * 弹出自定义CustomLoadDialog
//	 * @param context			上下文
//	 * @param message			提示
//	 * @param cancelable		是否按返回键取消 true 可以退出， false 禁止退出
//	 * @param cancelListener	按下返回键监听
//	 * @return
//	 */
//	@MainThread
//	public static CustomLoadDialog show(Context context, CharSequence message, boolean cancelable, OnCancelListener cancelListener) {
//
//		dismissDialog();
//
//		if(context == null) {
//			return null;
//		}
//
//		customLoadDialog = new CustomLoadDialog(context, R.style.CustomProgress);
//		// 设置显示文字
//		customLoadDialog.setMessage(message);
//		// 按返回键是否取消
//		customLoadDialog.setCancelable(cancelable);
//		// 监听返回键处理
//		customLoadDialog.setOnCancelListener(cancelListener);
//
//		customLoadDialog.show();
//		return customLoadDialog;
//	}
//
//	public static void dismissDialog() {
//		if(customLoadDialog != null && customLoadDialog.isShowing()) {
//			customLoadDialog.dismiss();
//		}
//		customLoadDialog = null;
//	}
//
//	private final DialogCustomLoadBinding binding;
//	private final ObservableInt messageVisible	= new ObservableInt(View.GONE);
//	private final ObservableField<CharSequence> message = new ObservableField<>();
//
//	private final int interval = 100;
//
//	private int		rotation = 45;
//	private boolean isStop	 = false;
//
//	private CustomLoadDialog(Context context) {
//		this(context, 0);
//	}
//
//	private CustomLoadDialog(Context context, int theme) {
//		super(context, theme);
//		binding = DialogCustomLoadBinding.inflate(LayoutInflater.from(context));
//		binding.setCustomLoadDialog(this);
//	}
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(binding.getRoot());
//
//		WindowManager.LayoutParams windowLP = getWindow().getAttributes();
//		// 设置居中
//		windowLP.gravity = Gravity.CENTER;
//		// 设置背景层透明度
//		windowLP.dimAmount = 0.0f;
//		getWindow().setAttributes(windowLP);
//	}
//
//	@Override
//	public void show() {
//		super.show();
//		isStop = false;
//		binding.loadIcon.postDelayed(this::loadAnimation, interval);
//	}
//
//	@Override
//	public void dismiss() {
//		isStop = true;
//		super.dismiss();
//	}
//
//	private void loadAnimation() {
//		if(isStop) {
//			return;
//		}
//		binding.loadIcon.postDelayed(this::loadAnimation, interval);
//		binding.loadIcon.setRotation(rotation);
//		rotation += 45;
//		rotation = rotation % 360;
//	}
//
//	/**
//	 * 提供设置加载对话框的底部消息文案设置
//	 * @param message	需要显示在加载对话框底部的文字
//	 * @return	返回加载对话框实体
//	 */
//	public CustomLoadDialog setMessage(CharSequence message) {
//		boolean isEmpty = StringUtil.isEmpty(message);
//		this.messageVisible.set(isEmpty ? View.GONE : View.VISIBLE);
//		this.message.set(isEmpty ? "" : message);
//		return this;
//	}
//
//	/**
//	 * 提供给dataBind使用的UI属性绑定
//	 * @return	绑定观察者属性:文本显示Visible
//	 */
//	public ObservableInt getMessageVisible() {
//		return messageVisible;
//	}
//
//	/**
//	 * 提供给dataBind使用的UI属性绑定
//	 * @return	绑定观察者属性:展示的文本
//	 */
//	public ObservableField<CharSequence> getMessage() {
//		return message;
//	}
//
//}
