package com.qire.common.support;

import com.qire.common.support.base.BaseDialog;

import java.util.Stack;

/**
 * dialog弹窗管理，统一构建弹窗。绑定activity生命周期
 */
public class DialogManage {

    /**
     * 屏蔽构造类
     */
    private DialogManage() {
        throw new UnsupportedOperationException("不需要初始化这个类");
    }

    /**
     * 是否显示
     * @param dialog 目标对话框
     * @return 如果不为空且是显示的状态为 true 否则 false
     */
    public static boolean isShowing(BaseDialog dialog) {
        return dialog != null && dialog.isShowing();
    }

    /**
     * 是否已存在
     * @param dialogClass 对话框类型
     * @return 存在为 true 否则 false
     */
    public static boolean exist(Class<? extends BaseDialog> dialogClass) {
        return false;
    }

    /**
     * dialog栈
     */
    private static final Stack<BaseDialog> dialogStack = new Stack<>();

    /**
     * 入栈
     * @param dialog
     */
    public static void add(BaseDialog dialog) {
        if(!dialogStack.contains(dialog)) {
            dialogStack.add(dialog);
        }
    }

    /**
     * 出栈
     * @param dialog
     */
    public static void remove(BaseDialog dialog) {
        if(dialogStack.contains(dialog)) {
            dialogStack.remove(dialog);
        }
    }

    /**
     * 最新的对话框
     * @return
     */
    public BaseDialog lastDialog() {
        return dialogStack.isEmpty() ? null : dialogStack.lastElement();
    }

}
