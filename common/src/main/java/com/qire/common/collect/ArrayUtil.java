package com.qire.common.collect;

public class ArrayUtil {

    /**
     * 判断数组是否为空
     * @param array 待检查数组
     * @return true 为 null 或 长度为0 ,否则false
     */
    public static <T> boolean isEmpty(T[] array) {
        return array == null || array.length == 0;
    }

    /**
     * 判断数组是否不为空
     * @param array 判定对象
     * @return 容器为null或者没有数据为false,否则true
     */
    public static <T> boolean notEmpty(T[] array) {
        return !isEmpty(array);
    }

    /**
     * 索引是否在目标容器范围内
     * @param array 判断对象
     * @param index 指定索引
     * @return 如果容器为 null 或 index 不在容器可取范围内为 false，否则为 true
     */
    public static boolean range(Object[] array, int index) {
        if(array == null) {
            return false;
        }
        return index >= 0 && index < array.length;
    }

    /**
     * 获取数组大小，如果数组为 null 则返回0
     * @param array 目标数组
     * @param <T> 数组类型
     * @return 数组为null 则返回 0  否则返回数组 {@code array.length}
     */
    public static <T> int size(T[] array) {
        return array == null ? 0 : array.length;
    }

    /**
     * 获得指定对象在数组中的索引位置，从0开始
     * @param array 数组
     * @param object 指定对象
     * @param <T>
     * @return 返回所在位置索引，如果数组不包括对象或数组为 {@code null} 则返回 -1
     */
    public static int indexOf(Object[] array, Object object) {
        if(array == null) {
            return -1;
        }
        for(int index = 0; index < array.length; index++) {
            if(array[index].equals(object)) {
                return index;
            }
        }
        return -1;
    }
}
