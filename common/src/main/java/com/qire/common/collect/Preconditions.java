package com.qire.common.collect;

/**
 * 准备条件判断
 */
public class Preconditions {

    /**
     * 检查 value 是否为负
     * @param value 判断的值
     * @param name 名称，用来提示异常
     * @return 如果值为负则抛出异常，否则返回原值
     */
    public static int checkNonNegative(int value, String name) {
        if (value < 0) {
            throw new IllegalArgumentException(name + " 不能为负,实际值为: " + value);
        }
        return value;
    }

    /**
     * 判断引用是否为空
     * @param reference 引用实例
     * @param <T>
     * @return 为空则抛出异常否则返回原引用
     */
    public static <T> T checkNotNull(T reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
        return reference;
    }

}
