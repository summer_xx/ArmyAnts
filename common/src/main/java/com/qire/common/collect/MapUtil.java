package com.qire.common.collect;

import java.util.Map;

public class MapUtil {

    public static boolean isEmpty(final Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    public static boolean notEmpty(final Map<?, ?> map){
        return !isEmpty(map);
    }


}
