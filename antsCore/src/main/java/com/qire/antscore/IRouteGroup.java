package com.qire.antscore;

import com.qire.antscore.entity.RouteMeta;

/**
 * 路由映射表-装载器接口（按组装载路由表）
 */
public interface IRouteGroup extends ILoader<String, RouteMeta> {
}
