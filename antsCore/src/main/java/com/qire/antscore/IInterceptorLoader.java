package com.qire.antscore;

import com.qire.antscore.entity.RulesMeta;

/**
 * 拦截器-装载器接口
 */
public interface IInterceptorLoader extends ILoader<Integer, RulesMeta> {
}
