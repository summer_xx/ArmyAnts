package com.qire.antscore;

import java.util.Map;

public interface ILoader<K, V> {
    void loadInto(Map<K, V> atlas);
}
