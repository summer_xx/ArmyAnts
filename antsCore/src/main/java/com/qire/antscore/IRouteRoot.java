package com.qire.antscore;

/**
 * 分组映射表-装载器接口（装载分组信息）
 */
public interface IRouteRoot extends ILoader<String, Class<? extends IRouteGroup>> {
}
