package com.qire.antscore.constant;

import com.qire.antscore.annotation.RouteMapping;

import javax.lang.model.SourceVersion;

public class ProcessorConfig {

    // 路由注解处理器关注的注解类型全类名
    public static final String ANNOTATION_TYPE_ROUTE = RouteMapping.class.getCanonicalName();
    // 拦截器注解处理器关注的注解类型全类名
    public static final String ANNOTATION_TYPE_INTERCEPTOR = "com.qire.antscore.annotation.InterceptorMapping";//Interceptor.class.getCanonicalName();

    /**
     * 外部传入注解处理器的属性名称：模块名属性的key，
     * 用于gradle中javaCompileOptions.annotationProcessorOptions.arguments 内容对象的key名
     * 例：javaCompileOptions.annotationProcessorOptions.arguments = [moduleName: project.getName()]，moduleName既是OPTIONS_MODULE_NAME的值。
     */
    public static final String OPTIONS_MODULE_NAME = "moduleName";

    /**
     * 注解处理器支持源码版本
     */
    public static final SourceVersion SOURCE_VERSION = SourceVersion.latestSupported();

}
