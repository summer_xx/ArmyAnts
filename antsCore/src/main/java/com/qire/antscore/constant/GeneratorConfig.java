package com.qire.antscore.constant;

import com.qire.antscore.IInterceptorLoader;
import com.qire.antscore.IRouteGroup;
import com.qire.antscore.IRouteRoot;

/**
 * 代码生成器配置文件
 */
public class GeneratorConfig {


    /**
     * 生成代码文件存放包名
     */
    public static final String PACKAGE_OF_GENERATE_FILE = "com.qire.antsrouter.routes";

    /**
     * 分组映射表-装载器接口全名
     */
    public static final String I_ROUTE_ROOT = IRouteRoot.class.getCanonicalName();
    /**
     * 路由映射表-装载器接口全名
     */
    public static final String I_ROUTE_GROUP = IRouteGroup.class.getCanonicalName();
    /**
     * 拦截器-装载器接口全名
     */
    public static final String I_INTERCEPTOR_LOADER = IInterceptorLoader.class.getCanonicalName();
    /**
     * IRouteRoot实现类生成使用的类名前缀
     */
    public static final String CLASS_ROOT_PREFIX = "AntsRouter_Root_";
    /**
     * IRouteGroup实现类生成使用的类名前缀
     */
    public static final String CLASS_GROUP_PREFIX = "AntsRouter_Group_";
    /**
     *
     */
    public static final String CLASS_INTERCEPTOR_PREFIX = "AntsRouter_Interceptor_";
    /**
     * 接口方法名
     */
    public static final String METHOD_LOAD_INTO = "loadInto";



}
