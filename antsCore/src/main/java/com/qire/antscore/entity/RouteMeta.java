package com.qire.antscore.entity;

import com.qire.antscore.annotation.RouteMapping;

import javax.lang.model.element.Element;

/**
 * 路由元数据
 */
public class RouteMeta {

    public enum Type {
        ACTIVITY,
        FRAGMENT,
        FRAGMENT_X,
        FRAGMENT_V4,
        ISERVICE
    }

    private Type type;

    /**
     * 节点（Activity）
     */
    private Element element;
    /**
     * 注解使用的类对象
     */
    private Class<?> destination;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 路由组
     */
    private String group;

    public static RouteMeta build(Type type, Class<?> destination, String path, String group) {
        return new RouteMeta(type, null, destination, path, group);
    }

    public RouteMeta() { }

    public RouteMeta(Type type, RouteMapping routeMapping, Element element) {
        this(type, element, null, routeMapping.path(), routeMapping.group());
    }

    public RouteMeta(Type type, Element element, Class<?> destination, String path, String group) {
        this.type = type;
        this.element = element;
        this.destination = destination;
        this.path = path;
        this.group = group;
    }


    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    public <T> Class<? extends T> getDestination() {
        return (Class<? extends T>) destination;
    }

    public void setDestination(Class<?> destination) {
        this.destination = destination;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

}
