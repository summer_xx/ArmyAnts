package com.qire.antscore.entity;

/**
 * 规则元数据
 */
public class RulesMeta {

    /**
     * 规则适用组群
     */
    private String[] group;

    /**
     * 注解使用的类对象
     */
    private Class<?> ruleClass;

    private RulesMeta(String[] group,Class<?> ruleClass){
        this.group = group;
        this.ruleClass = ruleClass;
    }

    public static RulesMeta build(String[] group,Class ruleClass){
        return new RulesMeta(group,ruleClass);
    }


    public String[] getGroup(){
        return group;
    }

    public <T>Class<T> getRuleClass() {
        return (Class<T>) ruleClass;
    }

}