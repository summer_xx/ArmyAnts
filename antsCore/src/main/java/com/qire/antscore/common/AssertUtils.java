package com.qire.antscore.common;

import java.util.Collection;
import java.util.Map;

/**
 * 断言检测工具
 */
public class AssertUtils {

    public static boolean isEmpty(CharSequence cs) {
        return cs == null || cs.length() == 0;
    }

    public static boolean notEmpty(CharSequence cs){
        return !isEmpty(cs);
    }

    public static boolean isEmpty(Collection<?> coll) {
        return coll == null || coll.isEmpty();
    }

    public static boolean notEmpty(Collection<?> coll){
        return !isEmpty(coll);
    }

    public static boolean isEmpty(final Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    public static boolean notEmpty(final Map<?, ?> map){
        return !isEmpty(map);
    }

    public static boolean isEmpty(Object[] objects) {
        return objects == null || objects.length == 0;
    }

    public static boolean notEmpty(Object[] objects) {
        return !isEmpty(objects);
    }

    public static boolean isNull(Object object) {
        return object == null;
    }

    public static boolean nonNull(Object object) {
        return object != null;
    }

}