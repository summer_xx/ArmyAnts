package com.qire.antscore.common;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

//import java.util.concurrent.BlockingQueue;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.LinkedBlockingQueue;

public class AntsThreadBuilder {

    // cpu内核数量
    private static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    // 保持生存时间，存活30秒回收线程
    private static final long KEEP_ALIVE_TIME = 30L;
    // 保持活着的时间单位,秒
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

//    public static ExecutorService newSimplePoolExecutor(int corePoolSize) {
//        BlockingQueue<Runnable> taskQueue = new LinkedBlockingQueue<>();
//        ExecutorService executorService = new ThreadPoolExecutor(
//                NUMBER_OF_CORES,
//                NUMBER_OF_CORES*2,
//                KEEP_ALIVE_TIME,
//                KEEP_ALIVE_TIME_UNIT,
//                taskQueue,
//                new AntsThreadFactory("Simple"),
//                new DefaultRejectedExecutionHandler());
//        return executorService;
//    }

    public static AntsThreadFactory newAntsThreadFactory(String threadName){
        return new AntsThreadFactory(threadName);
    }


    public static ThreadPoolExecutor defaultExecutor(){
        return DefaultPoolBuilder.EXECUTOR;
    }

    private static class DefaultPoolBuilder {
        private static final ThreadPoolExecutor EXECUTOR = newDefaultPoolExecutor(NUMBER_OF_CORES);
        private static ThreadPoolExecutor newDefaultPoolExecutor(int corePoolSize) {
            if (corePoolSize == 0) {
                return null;
            }
            //核心线程和最大线程都是cpu核心数+1
            int maxCorePoolSize = NUMBER_OF_CORES + 1;

            corePoolSize = Math.min(corePoolSize, maxCorePoolSize);
            ThreadPoolExecutor executor = new ThreadPoolExecutor(
                    corePoolSize,
                    corePoolSize,
                    KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT,
                    new ArrayBlockingQueue<>(64),
                    new AntsThreadFactory("AntsRouter"));//"AntsRouter #";
            //核心线程也会被销毁
            executor.allowCoreThreadTimeOut(true);
            return executor;
        }
    }

    public static class AntsThreadFactory implements ThreadFactory {
        private final String namePrefix;
        private final AtomicInteger nextId = new AtomicInteger(1);
        // 定义线程组名称，在 jstack命令 问题排查时，非常有帮助
        AntsThreadFactory(String whatGroupName) {
            namePrefix = "From AntsThreadFactory's " + whatGroupName + "-Worker-";
        }

        @Override
        public Thread newThread(Runnable task) {
            String name = namePrefix + nextId.getAndIncrement();
            Thread thread = new Thread(null, task, name, 0);
            System.out.println(thread.getName());
            return thread;
        }
    }

}
