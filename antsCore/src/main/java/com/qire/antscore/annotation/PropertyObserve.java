package com.qire.antscore.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ViewModel属性观察者注解，用于绑定属性的观察者
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
public @interface PropertyObserve {

    /**
     * 属性名称
     * @return String
     */
    String name();

    /**
     * 绑定方式
     */
    BindType bindType() default BindType.ObserveForever;

    enum BindType {
        ObserveForever("observeForever"),
        Observe("observe");
        public final String action;

        BindType(String action){
            this.action = action;
        }

    }

}
