package com.qire.antscore.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * DataWarehouse 数据访问异常观察者，用于数据访问时，如API调用、本地数据库读取发生的异常捕获统一处理的观察者绑定
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
public @interface DalExceptionObserve {

    /**
     * 关注目标资源URI/url
     * @return String
     */
    String[] tags();

}
