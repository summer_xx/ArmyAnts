package com.qire.antscore.annotation;

/**
 * 深度连接
 */
public @interface DeepLink {

    String path() default "";

}
