package com.qire.antscore.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.CLASS)
public @interface InterceptorMapping {

    /**
     * 拦截器优先级
     */
    int priority() default 0;
    /**
     * 拦截器的名称
     */
    String name() default "";

    /**
     * 拦截器的组别
     */
    String[] group() default "";

}
