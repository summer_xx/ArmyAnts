package com.qire.sample.provider.impl;

import com.qire.antscore.annotation.DeepLink;
import com.qire.antscore.annotation.RouteMapping;
import com.qire.antsdeeplink.DeepLinkUri;
import com.qire.antsrouter.inject.Autowired;
import com.qire.sample.provider.ITestProviderService;

@DeepLink(path = "http://summer/{*}")
@RouteMapping(path = "/provider/helloWord")
public class TestProviderService implements ITestProviderService {

    @Autowired
    String name;

    final String sex;

    public TestProviderService(String sex) {
        this.sex = sex;
    }

    @Override
    public void helloWord() {
        System.out.println("hello word!" + name + "，性別：" + sex);
    }

    public static boolean doAction(DeepLinkUri deepLinkUri) {
        new TestProviderService("未知").helloWord();
        return true;
    }
    
}
