package com.qire.sample.deeplink;

import android.widget.Toast;

import com.qire.antscore.annotation.DeepLink;
import com.qire.antsdeeplink.DeepLinkHandler;
import com.qire.antsdeeplink.DeepLinkUri;
import com.qire.common.SummerApp;
import com.qire.common.utils.Logger;

//@DeepLink(path = "example://app/homePage")
@DeepLink(path = "example://summer/homePage1")
public class ActionDeepLinkHandler1 implements DeepLinkHandler {
    @Override
    public boolean toProcess(DeepLinkUri deepLinkUri) {
//        String path = deepLinkUri.processorPath().equals("HomeActviity5") ? "/app/homePage" : "";
        Logger.d("AntsDeepLink===>ActionDeepLinkHandler");
        Toast.makeText(SummerApp.summerApp(), "ActionDeepLinkHandler", Toast.LENGTH_SHORT).show();
        return true;
    }


    public boolean action1(DeepLinkUri deepLinkUri) {
        return false;
    }
    public boolean action2(DeepLinkUri deepLinkUri) {
        return false;
    }
    public static boolean action5(DeepLinkUri deepLinkUri) {
        return false;
    }
    public static boolean action111(DeepLinkUri deepLinkUri) {
        return false;
    }

}
