package com.qire.sample.deeplink;

import android.widget.Toast;

import com.qire.antscore.annotation.DeepLink;
import com.qire.antsdeeplink.DeepLinkHandler;
import com.qire.antsdeeplink.DeepLinkUri;
import com.qire.antsrouter.AntsRouter;
import com.qire.antsrouter.card.FloorCard;
import com.qire.common.SummerApp;
import com.qire.common.utils.Logger;

@DeepLink(path = "example://summer/homePage")
public class ActionDeepLinkHandler implements DeepLinkHandler {
    @Override
    public boolean toProcess(DeepLinkUri deepLinkUri) {
//        String path = deepLinkUri.processorPath().equals("HomeActviity5") ? "/app/homePage" : "";
        Logger.d("AntsDeepLink===>ActionDeepLinkHandler");
        Toast.makeText(SummerApp.summerApp(), "ActionDeepLinkHandler", Toast.LENGTH_SHORT).show();
        return true;
    }


    public boolean action1(DeepLinkUri deepLinkUri) {
        Toast.makeText(SummerApp.summerApp(), "ActionDeepLinkHandler", Toast.LENGTH_SHORT).show();
        return false;
    }
    public boolean action2(DeepLinkUri deepLinkUri) {
        return false;
    }
    public boolean action3(DeepLinkUri deepLinkUri) {
        return false;
    }
    public boolean action4(DeepLinkUri deepLinkUri) {
        return false;
    }
    public static boolean action5(DeepLinkUri deepLinkUri) {
        return false;
    }

}
