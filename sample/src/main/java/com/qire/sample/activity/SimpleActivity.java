package com.qire.sample.activity;

import android.os.Bundle;

import com.qire.antsbinder.viewModel.ViewModelFactory;
import com.qire.antscore.annotation.DeepLink;
import com.qire.antscore.annotation.RouteMapping;
import com.qire.antsrouter.inject.Autowired;
import com.qire.common.inject.ViewModelInjector;
import com.qire.common.support.base.BaseViewModel;
import com.qire.sample.model.viewModel.ISimpleViewModel;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

/**
 * 简单使用示例
 */
@RouteMapping(path = "/app/simplePage")
@DeepLink(path = "example://summer/SimpleActivity")
public class SimpleActivity extends FragmentActivity {

    @Autowired(injector = ViewModelInjector.class)
    private BaseViewModel<ISimpleViewModel> viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelFactory.crate(this, BaseViewModel.class);
    }
}
