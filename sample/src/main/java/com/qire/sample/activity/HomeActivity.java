package com.qire.sample.activity;

import android.os.Bundle;

import com.qire.antsbinder.viewModel.ViewModelFactory;
import com.qire.antscore.annotation.RouteMapping;
import com.qire.antsrouter.inject.Autowired;
import com.qire.sample.R;
import com.qire.sample.databinding.ActivityHomeBinding;
import com.qire.sample.model.viewModel.impl.HomeViewModel;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;

@RouteMapping(path = "/app/homePage")
public class HomeActivity extends FragmentActivity {

    @Autowired
    String msg1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        HomeViewModel viewModel = ViewModelFactory.crate(this, HomeViewModel.class);

        ActivityHomeBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        binding.setHomeViewModel(viewModel.buildProxy());
        binding.setLifecycleOwner(this);

        viewModel.loadPage();

        viewModel.set("msg1", msg1);

//        IHomeData homeData = WarehouseFactory.create(viewModel,IHomeData.class);
//
//        homeData.loadPage();


//        ActivityHomeBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_home);
//        AntsViewModel viewModel = ViewModelFactory.crate(this, AntsViewModel.class);
//        binding.setHomeViewModel((IHomeViewModel) ViewModelFactory.crate(this, AntsViewModel.class).buildProxy(IHomeViewModel.class));
//
//        binding.getRoot().postDelayed(()->viewModel2.put("msg","hahahaha"),1000);
//
//        IHomeData homeData = WarehouseFactory.create(new HomePresenter(),IHomeData.class);


//        homeData.getTitle("测试名","页面");

//        String msg = homeData.getMsg();
//        viewModel2.put("msg",msg);
    }
}
