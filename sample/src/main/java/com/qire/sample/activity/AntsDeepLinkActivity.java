package com.qire.sample.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.qire.antsdeeplink.AntsDeepLink;
import com.qire.antsdeeplink.DeepLinkUri;
import com.qire.common.utils.Logger;

public class AntsDeepLinkActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = new View(this);
        setContentView(root);

        Logger.d(String.format("AntsDeepLink===>tag:%s,  info:%s", "AntsDeepLinkActivity", "DeepLink 进入"));

        Intent intent = getIntent();

        String action = intent.getAction();
        Logger.d(String.format("AntsDeepLink===>tag:%s,  action:%s", "AntsDeepLinkActivity", action));


        Uri deepLinkUri = intent.getData();
        Logger.d("AntsDeepLink===>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        Logger.d(String.format("AntsDeepLink===>tag:%s,  normalizeScheme:%s", "DeepLinkUri", deepLinkUri.normalizeScheme()));
        Logger.d(String.format("AntsDeepLink===>tag:%s,  Scheme:%s", "DeepLinkUri", deepLinkUri.getScheme()));
        Logger.d(String.format("AntsDeepLink===>tag:%s,  SchemeSpecificPart:%s", "DeepLinkUri", deepLinkUri.getSchemeSpecificPart()));
        Logger.d(String.format("AntsDeepLink===>tag:%s,  Authority:%s", "DeepLinkUri", deepLinkUri.getAuthority()));
        Logger.d(String.format("AntsDeepLink===>tag:%s,  UserInfo:%s", "DeepLinkUri", deepLinkUri.getUserInfo()));
        Logger.d(String.format("AntsDeepLink===>tag:%s,  Host:%s", "DeepLinkUri", deepLinkUri.getHost()));
        Logger.d(String.format("AntsDeepLink===>tag:%s,  Port:%s", "DeepLinkUri", deepLinkUri.getPort()));
        Logger.d(String.format("AntsDeepLink===>tag:%s,  Path:%s", "DeepLinkUri", deepLinkUri.getPath()));
        Logger.d(String.format("AntsDeepLink===>tag:%s,  LastPathSegment:%s", "DeepLinkUri", deepLinkUri.getLastPathSegment()));
        Logger.d(String.format("AntsDeepLink===>tag:%s,  Query:%s", "DeepLinkUri", deepLinkUri.getQuery()));
        Logger.d(String.format("AntsDeepLink===>tag:%s,  EncodedQuery:%s", "DeepLinkUri", deepLinkUri.getEncodedQuery()));
        Logger.d(String.format("AntsDeepLink===>tag:%s,  Fragment:%s", "DeepLinkUri", deepLinkUri.getFragment()));

        for(String pathSegment : deepLinkUri.getPathSegments()) {
            Logger.d(String.format("AntsDeepLink===>tag:%s,  pathSegment:%s", "DeepLinkUri", pathSegment));
        }

        for(String name : deepLinkUri.getQueryParameterNames()) {
            Logger.d(String.format("AntsDeepLink===>tag:%s,  QueryParameter:%s", "DeepLinkUri", name + "  =  " + deepLinkUri.getQueryParameter(name)));
        }

        Logger.d("AntsDeepLink===>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

        DeepLinkUri deepLinkUri1 = new DeepLinkUri.Builder("qire", "com.qire.com")
                .setUserInfo("summer_xx", "xss11111")
                .setPort(300)
                .addPathSegment("HomeActivity")
                .addQueryParam("Summer", "sxx")
                .setFragment("userFragment")
                .build();
        Logger.d(String.format("AntsDeepLink===>tag:%s,  deepLinkUri:%s", "AntsDeepLink", deepLinkUri1));

        Logger.d("AntsDeepLink===>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

        AntsDeepLink.DISPATCHER.receiveFrom(intent);

        root.post(this::finish);
    }

    @Override
    public void finish() {
        super.finish();
        Logger.d(String.format("AntsDeepLink===>tag:%s,  msg:%s", "AntsDeepLinkActivity", "finish"));
    }

}
