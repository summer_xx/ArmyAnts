package com.qire.sample.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.qire.antscore.annotation.RouteMapping;
import com.qire.antsrouter.AntsRouter;
import com.qire.antsrouter.card.FloorCard;
import com.qire.antsrouter.card.ServiceCard;
import com.qire.sample.R;
import com.qire.sample.provider.ITestProviderService;
import com.qire.sample.provider.impl.TestProviderService;

import androidx.annotation.Nullable;

@RouteMapping(path = "/app/mainPage")
public class MainActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void gotoHome(View view) {
//        AntsRouter.ROUTER.buildCard("/app/homePage").navigation(0);
        AntsRouter.ROUTER.<FloorCard>buildCard("/app/homePage")
                .withTransition(R.anim.activity_open_enter, R.anim.activity_open_exit)
                .navigation();
    }

    public void testOnClick(View view) {
        long time = System.currentTimeMillis();

//        summer://provider/helloWord?name=summer
        TestProviderService service = AntsRouter.ROUTER.<ServiceCard<TestProviderService>>buildCard("/provider/helloWord")
                .setInitArgs("男")
                .withData("name", "summer")
                .navigation()
                .get();
        service.helloWord();
        System.out.println("hello 耗時：" + (System.currentTimeMillis() - time));
    }

}
