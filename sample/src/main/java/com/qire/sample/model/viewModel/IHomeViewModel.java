package com.qire.sample.model.viewModel;

import com.qire.antsbinder.viewModel.annotation.ViewModelProperty;
import com.qire.common.support.base.IViewModel;
import com.qire.sample.BR;

import androidx.lifecycle.MutableLiveData;

public interface IHomeViewModel extends IViewModel {

    int variableId = BR.homeViewModel;

    @ViewModelProperty(name = "msg")
    MutableLiveData<String> getMsg();

    @ViewModelProperty(name = "msg1")
    MutableLiveData<String> getMsg1();

}
