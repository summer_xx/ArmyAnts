package com.qire.sample.model.viewModel;

import com.qire.antsbinder.viewModel.annotation.ViewModelProperty;
import com.qire.common.support.base.IViewModel;
import com.qire.sample.BR;

import androidx.lifecycle.MutableLiveData;

public interface IMainViewModel extends IViewModel {

    int variableId = BR.mainViewModel;

//    // 使用默认方法投递BindView的variableId,但由于默认方法的动态代理处理需要支持到android 26以上目前放弃
//    @Override
//    default int variableId(){
//        return BR.mainViewModel;
//    }

    @ViewModelProperty(name = {"content","msg"},type = ViewModelProperty.Type.SET)
    void setContent(String content, String msg);

    @ViewModelProperty(name = "content")
    MutableLiveData<String> getContent();



}
