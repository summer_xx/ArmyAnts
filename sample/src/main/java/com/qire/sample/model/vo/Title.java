package com.qire.sample.model.vo;

import com.qire.antsbinder.viewModel.AntsViewModel;
import com.qire.common.support.base.BaseVO;

public class Title extends BaseVO {

    public String update;

    @Override
    public void updateModel(AntsViewModel viewModel) {
        viewModel.put("msg",update);
    }
}
