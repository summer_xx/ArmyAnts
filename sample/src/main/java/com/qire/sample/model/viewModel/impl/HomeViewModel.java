package com.qire.sample.model.viewModel.impl;

import com.qire.antsbinder.dal.DataService;
import com.qire.antsbinder.dal.annotation.StrategyAnnotation;
import com.qire.antsbinder.viewModel.AntsViewModel;
import com.qire.common.dal.WarehouseFactory;
import com.qire.common.dal.WarehouseFactoryVo;
import com.qire.sample.dal.IHomeData;
import com.qire.sample.model.viewModel.IHomeViewModel;
import com.qire.sample.model.vo.Title;

public class HomeViewModel extends AntsViewModel<IHomeViewModel> {

    IHomeData data =  WarehouseFactoryVo.create(this,IHomeData.class);

    public HomeViewModel(){
    }

    public void loadPage(){
        data.loadPage();
    }

    private class HomeDataService extends DataService {

        IHomeData data =  WarehouseFactory.create(this,IHomeData.class);

        public void loadPage(){
            data.loadPage();
        }
        @StrategyAnnotation(event="https://api.qrcomic.com/v5/wow/config?webdebug=1")
        public void callBackConfig(Title vo, boolean isCache){
            put("msg", vo.update);
        }

    }

}
