package com.qire.sample.dal;

import com.qire.antsbinder.dal.DataWarehouse.MethodType;
import com.qire.antsbinder.dal.annotation.DataAccess;
import com.qire.sample.model.vo.Title;

public interface IHomeData {

    @DataAccess(url ="https://api.qrcomic.com/v5/wow/config?webdebug=1",methodType = MethodType.GET)
    Title loadPage();

}
