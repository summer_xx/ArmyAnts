package com.qire.antsdeeplink;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

public class AntsDeepLinkActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = new View(this);
        setContentView(root);
        Intent intent = getIntent();

        AntsDeepLink.DISPATCHER.receiveFrom(intent);

        root.post(this::finish);
    }

}
