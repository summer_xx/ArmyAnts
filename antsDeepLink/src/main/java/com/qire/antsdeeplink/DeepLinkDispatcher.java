package com.qire.antsdeeplink;

import com.qire.antscore.common.AssertUtils;

import java.util.List;

public class DeepLinkDispatcher {

    public void dispatchFrom(DeepLinkUri deepLinkUri, List<DeepLinkDispatchRegistry> dispatchRegistryList) {
        if(AssertUtils.nonNull(deepLinkUri)) {

            String scheme = deepLinkUri.scheme();

//            DeepLinkFilter filter;
//            if(filter.onFilter(deepLinkUri)) {
//                return;
//            }

            for(DeepLinkDispatchRegistry dispatchRegistry : dispatchRegistryList) {
                if(dispatchRegistry.matchScheme(scheme)) {
                    DeepLinkHandler processor = dispatchRegistry.findHandler(deepLinkUri);
                    if(processor != null && processor.toProcess(deepLinkUri)) {
                        return;
                    }
                }
            }
        }
    }

}
