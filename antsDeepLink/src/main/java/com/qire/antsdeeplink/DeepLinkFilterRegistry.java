package com.qire.antsdeeplink;

/**
 * 深度连接过滤器注册表
 */
public abstract class DeepLinkFilterRegistry {

    public abstract boolean matchHost(String host);

    public abstract DeepLinkFilter findFilter(String path);
}
