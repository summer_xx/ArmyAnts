package com.qire.antsdeeplink;

public interface DeepLinkFilter {

    boolean onFilter(DeepLinkUri deepLinkUri);

}
