package com.qire.antsdeeplink;

public abstract class DeepLinkDispatchRegistry {

    public abstract boolean matchScheme(String scheme);

    public abstract DeepLinkHandler findHandler(DeepLinkUri deepLinkUri);

}
