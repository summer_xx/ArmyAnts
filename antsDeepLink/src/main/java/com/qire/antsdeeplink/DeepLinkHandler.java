package com.qire.antsdeeplink;

/**
 * 深度连接处理器
 */
public interface DeepLinkHandler {

    boolean toProcess(DeepLinkUri deepLinkUri);

}
