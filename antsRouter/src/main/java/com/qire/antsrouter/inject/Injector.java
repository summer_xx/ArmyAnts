package com.qire.antsrouter.inject;

import com.qire.antsrouter.card.Postcard;

import java.lang.reflect.Field;

public interface Injector<T extends Postcard,V> {

    void inject(Field field,T postcard, V injectedTarget);

}
