package com.qire.antsrouter.inject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Autowired {

    /**
     * 别名
     * @return 返回注入时使用的别名
     */
    String name() default "";

    /**
     * 如果是必须的，传递注入值为null时会抛出空指针异常，这个检测会发生在注入时，方便问题定位
     * @return true则表示该字段必须传递注入。
     */
    boolean required() default false;

    /**
     * 描述字段无意义
     * @return 展示使用
     */
    String desc() default "";

    /**
     * 注册器类型，可以自定义某个字段具体使用的注册行为。
     * @return
     */
    Class<? extends Injector> injector() default Injector.class;

}
