package com.qire.antsrouter.card;

import android.os.Handler;

import com.qire.antscore.common.AssertUtils;
import com.qire.antscore.entity.RouteMeta;
import com.qire.antsrouter.provider.IService;
import com.qire.antsrouter.utils.ClassUtils;

/**
 * 服务卡(IService跳卡)
 * @param <S> 具体服务类或接口。泛型支持在构建 card 时，通过描述泛型来达成类型检测与自动转换。
 */
public class ServiceCard<S extends IService> extends Postcard<S> {

    private S service;
    private Object[] initArgs;

    protected ServiceCard(RouteMeta routeMeta, Handler handler) {
        super(routeMeta, handler);
    }

    /**
     * 获得服务实体引用
     * @return 服务实体
     */
    public S get() {
        return service;
    }

    /**
     * 设置构造函数参数
     * @param initArgs 构造参数数组
     * @return
     */
    public ServiceCard<S> setInitArgs(Object... initArgs) {
        this.initArgs = initArgs;
        return this;
    }

    /**
     * 注入数据
     * @param fieldName 注入字段名
     * @param data      注入数据
     * @return 这张房卡
     */
    public ServiceCard<S> withData(String fieldName, Object data) {
        dataContainer.put(fieldName, data);
        return this;
    }

    @Override
    public <T extends S> void inject(T destTarget) {
        super.inject(destTarget);
    }

    @Override
    public ServiceCard<S> navigation() {
        // todo：需要扩展一个类型，即服务可能是全局唯一单例的。可能需要扩展一个缓存map和注解属性来配合完成。注解属性也可以有IService中提供方法来判定。

        service = ClassUtils.newInstance(routeMeta.<S>getDestination(), initArgs);
        if(AssertUtils.nonNull(service)) {
            inject(service);
        }
        return this;
    }

}
