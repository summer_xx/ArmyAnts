package com.qire.antsrouter.card;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CardBag {

    // 卡包：跳卡缓存，key为cardId,value为跳卡
    private static final Map<String, Postcard> cardMap = new LinkedHashMap<>();
//    static {
//        // 为起始Activity添加起始卡片
//        put(-1, new FloorCard(null, null));
//    }

    public static void put(int cardId, Postcard postcard) {
        put(String.valueOf(cardId), postcard);
    }

    public synchronized static void put(String cardId, Postcard postcard) {
        cardMap.put(cardId, postcard);
        //todo:插入后需要一个延时清理器，在一定时间后删除卡片，如果卡片已被取走，则退出清理器。
        timeOutCleanup();
    }

    /**
     * 通过cardId取卡
     * @param cardId int型卡ID
     * @param <T> 卡片实际类型
     * @return 对应的卡片，如果存在返回卡片否则返回null
     */
    public static <T extends Postcard>T takeCard(int cardId) {
        return takeCard(String.valueOf(cardId));
    }

    public synchronized static <T extends Postcard> T takeCard(String cardId) {
        T card = (T) cardMap.remove(cardId);

        // 为起始Activity返回一个卡片以解决注入问题
        return card == null ? (T) new FloorCard(null, null) : card;
    }

    /**
     * 超时清理,兜底，一般情况下可以不用清理
     */
    private static void timeOutCleanup() {
//        System.out.println(Thread.currentThread().getName() + " size = " + cardMap.size());

        long nowTime = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());

        Iterator<Map.Entry<String, Postcard>> iterator = cardMap.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, Postcard> entry = iterator.next();
            Postcard postcard = entry.getValue();

            long difference = nowTime - postcard.buildTime;
            if(difference < 300) {
                return;
            }

            iterator.remove();
        }

    }

} 
