package com.qire.antsrouter.card;

import android.os.Handler;

import com.qire.antscore.common.AssertUtils;
import com.qire.antscore.entity.RouteMeta;

public class PostcardFactory {

    public static final Postcard build(RouteMeta routeMeta, Handler handler) {
        if(AssertUtils.isNull(routeMeta)) {
            return new EmptyCard(null, null);
        }

        switch (routeMeta.getType()) {
            case ACTIVITY:
                return new FloorCard(routeMeta, handler);
            case FRAGMENT_X:
                return new RoomCard(routeMeta, handler);
            case ISERVICE:
                return new ServiceCard(routeMeta, handler);
            default:
                return new EmptyCard(routeMeta, handler);
        }
    }

    private static class EmptyCard extends Postcard {
        protected EmptyCard(RouteMeta routeMeta, Handler handler) {
            super(routeMeta, handler);
        }

        @Override
        public void inject(Object target) {}

        @Override
        public EmptyCard navigation() {
            return this;
        }
    }

}
