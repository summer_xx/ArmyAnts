package com.qire.antsrouter.utils;

import java.util.concurrent.CountDownLatch;

/**
 * 可取消线程栅栏
 */
public class CancelableCountDownLatch extends CountDownLatch {

    private String msg = "";
    private volatile boolean isCancel = false;

    public CancelableCountDownLatch(int count) {
        super(count);
    }

    /**
     * 当遇到特殊情况时，需要将计步器清0
     */
    public void cancel(String msg) {
        this.isCancel = true;
        this.msg = msg;
        while (getCount() > 0) {
            countDown();
        }
    }

    /**
     * 是否取消了拦截
     * @return
     */
    public boolean isCancel(){
        return isCancel;
    }

    /**
     * 取消拦截传递的消息
     * @return 消息文本
     */
    public String getMsg(){
        return msg;
    }
}