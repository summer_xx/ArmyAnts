package com.qire.antsrouter.utils;

import com.qire.antscore.common.AssertUtils;

public class StringUtils {

    public static String getDef(String str,String def){
        return AssertUtils.isEmpty(str) ? def : str;
    }

}
