package com.qire.antsrouter.lifecycleHandler;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/**
 * Activity生命周期处理程序
 */
public abstract class ActivityLifecycleHandler implements Application.ActivityLifecycleCallbacks {

    /**
     * 注册
     * @param application app实例
     */
    public void register(Application application){
        application.registerActivityLifecycleCallbacks(this);
    }
    /**
     * 注销
     * @param application app实例
     */
    public void unregister(Application application){
        application.unregisterActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {}

    @Override
    public void onActivityStarted(Activity activity) {}

    @Override
    public void onActivityResumed(Activity activity) {}

    @Override
    public void onActivityPaused(Activity activity) {}

    @Override
    public void onActivityStopped(Activity activity) {}

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}

    @Override
    public void onActivityDestroyed(Activity activity) {}
}