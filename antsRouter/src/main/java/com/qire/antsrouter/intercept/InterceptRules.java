package com.qire.antsrouter.intercept;

import android.content.Context;

import com.qire.antsrouter.card.Postcard;

/**
 * 拦截器规则接口，实现拦截器功能需要继承此接口，改接口修改需谨慎，由于注解编译模块需要依赖次接口构建，<br />
 * 且javaLib模块无法引用androidLib模块所以采用类全名字符串绑定关联，如果此处修改会引起编译部分出错，记得关联修改对应处
 * 参见{@link com.qire.antscompiler.generator.InterceptorCodeGenerator#INTERCEPT_RULES INTERCEPT_RULES}
 */
public interface InterceptRules {

    /**
     * 拦截器规则处理过程。<br />
     * 跳卡导航时，会先执行拦截器，拦截器会按组调用规则处理过程。
     */
    void process(Postcard postcard,Interceptor interceptor);

    /**
     * 在调用 MappingCache.init()初始化时，会调用到此方法
     * 参见{@link com.qire.antsrouter.MappingCache#init MappingCache.init()}。
     */
    void init(Context context);

} 
