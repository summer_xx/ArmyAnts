package com.qire.antsrouter.exception;

/**
 * AntsRouter抛出异常，为找到对应的路由元数据
 */
public class NoRouteFoundException extends RuntimeException {
    public NoRouteFoundException(String detailMessage) {
        super(detailMessage);
    }
}
